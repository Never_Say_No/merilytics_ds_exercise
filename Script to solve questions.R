# importing library and reading 
library(tidyr)
library(dplyr)
item_to_id = read.csv("item_to_id.csv")
purchase_history = read.csv("purchase_history.csv")

#install.packages("tidyr")
# separating the value in id column in rows
df = separate_rows(purchase_history , id , sep="[^[:alnum:].]+" , convert = TRUE)

# joining the table item_to_id with df
data = left_join(item_to_id, df, by = c("Item_id" = "id"))

# calculating the most_sold_item
most_sold_item = data %>% group_by(Item_name) %>% 
  summarise( no_of_item_sold = n()) %>% 
  arrange(desc(no_of_item_sold)) %>% head(1)

# calculating the USER_BOUGHT_MAX
USER_BOUGHT_MAX = data %>% group_by(user_id) %>% 
  summarise(number_of_iem_bought = n()) %>% 
  arrange(desc(number_of_iem_bought)) %>% head(1)

# calculating the most_frequest_trans
most_frequest_trans = data %>% group_by(user_id,Item_name) %>% 
  summarise(no_of_trans = n()) %>% arrange(desc(no_of_trans,Item_name)) %>% head(10)

#exporting the csv file 
write.csv(most_frequest_trans,'most_frequest_trans.csv',row.names=F)

